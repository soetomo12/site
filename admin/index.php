<?php include("../config.php"); ?>
<!DOCTYPE html>
<!--[if IE 8]><html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]>
<!--><html class="no-js" lang="en"><!--<![endif]-->
<head>

 	<!-- Basic Page Needs
  ================================================== -->
	<meta charset="utf-8">
	<title>S I T E</title>
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Mobile Specific Metas
  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  
	<!-- CSS
  ================================================== -->
	<!-- Bootstrap core CSS -->
	<link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<link rel="stylesheet" href="../css/base.css"/>
	<link rel="stylesheet" href="../css/skeleton.css"/>
	<link rel="stylesheet" href="../css/layout.css"/>
	<link rel="stylesheet" href="../css/settings.css"/>
	<link rel="stylesheet" href="../css/font-awesome.css" />
	<link rel="stylesheet" href="../css/owl.carousel.css"/>
	<link rel="stylesheet" href="../css/retina.css"/>
	<link rel="stylesheet" href="../css/colorbox.css"/>
	<link rel="stylesheet" href="../css/animsition.min.css"/>	
</head>

<?php
    if(isset($_GET['pesan'])){
      if($_GET['pesan'] == "berhasil"){
        echo "<body onload=berhasilBerhasilBerhasilHore()>";
      }
    }else{
      echo "<body>";
    }
  ?>

  <!-- cek apakah sudah login -->
  <?php 
  session_start();
  if($_SESSION['stat']!="login" OR $_SESSION['admin']!="TRUE"){
    header("location:UMAD.html");
  }
  ?>

<body class="bg-warning">	
	<!-- MENU
    ================================================== -->	
	
	<div class="header-top">
    <header class="cd-main-header">
      <a class="cd-logo animsition-link" href="index.php">S I T E [admin]</a>

      <ul class="cd-header-buttons">
        <li><a class="cd-nav-trigger" href="#cd-primary-nav"><span></span></a></li>
      </ul> <!-- cd-header-buttons -->
    </header>
    
    <nav class="cd-nav">
      <ul id="cd-primary-nav" class="cd-primary-nav is-fixed">
        <li>
          <a href="index.php" class="animsition-link">Home</a>
        </li>
        <li class="has-children">
          <a href="#">Halo, <?php echo $_SESSION['username']; ?>!</a>
          <ul class="cd-secondary-nav is-hidden">
              <li><a href="cekEvent.php">Cek Info Turnamen</a></li>
              <li><a href="cekmabar.php">Cek Info Mabar</a></li>
              <li><a href="logOut.php">Log Out</a></li>
          </ul>
        </li>
      </ul> <!-- primary-nav -->
    </nav> <!-- cd-nav -->  
  </div>

 <main class="cd-main-content">
  	<!-- HOME SECTION
    ================================================== -->
	
		<section class="section parallax-section parallax-section-padding-top-bottom-pagetop section-page-top-title">
			<div class="parallax-5"></div>
			<div class="fullscreen-title-home">S I T E</div>
			<div class="fullscreen-subtitle-home">Sistem Informasi Turnamen E-Sport</div>
			<div style="text-align: center; margin-top: 5%; margin-bottom: -5%">
        <a href="list.php" class="button-shortcodes text-size-4 text-padding-4 version-2">Lihat Semua Event & Lomba</a>
        <a href="tambahLivent.php" class="button-shortcodes text-size-4 text-padding-4 version-2">Tambah Info Turnamen</a>
			</div>
		</section>
		
	 <!-- SECTION
    ================================================== -->
    <section class="section" id="scroll-link">
			<div class="call-to-action-2">
				<div class="container">
					<div class="sixteen columns">
						<h6>Rekomendasi Event E-Sport</h6>
					</div>
				</div>
			</div>
		</section>	
		
		<div class="container mt-5">
	    <section>
	    <div class="container">
	      <div class="row align-items-center">
	        <div class="col-lg-6 order-lg-2">
	          <div class="p-5">
	            <img class="img-fluid rounded-circle" src="../img/M3.png" alt="">
	          </div>
	        </div>
	        <div class="col-lg-6 order-lg-1">
	          <div class="p-5">
	            <a href="index.php" style="color: black;"><h2 class="display-4">M3 World Championship</h2></a>
	            <p style="color:black">M3 Mobile Legends merupakan kejuaraan bergengsi yang diikuti oleh peserta dari seluruh dunia. Kompetisi ini memperebutkan uang senilai US$800 atau setara dengan Rp11 miliar. Ajang M3 Mobile Legends dimulai sejak tanggal 6 hingga 19 Desember 2021 di Singapura. Babak pembuka adalah fase grup M3 World Championships Mobile Legends sampai tanggal 9 Desember 2021. Sementara babak playoff dihelat 11 hingga 18 Desember 2021. Babak puncak atau pertandingan final menjadi penutup perhelatan M3 pada tanggal 19 Desember 2021 mendatang.</p>
	          </div>
	        </div>
	      </div>
	    </div>
	  </section>

	  <section>
	    <div class="container">
	      <div class="row align-items-center">
	        <div class="col-lg-6">
	          <div class="p-5">
	            <img class="img-fluid rounded-circle" src="../img/pmgc.jpg" alt="">
	          </div>
	        </div>
	        <div class="col-lg-6">
	          <div class="p-5">
	            <a href="#" style="color: black;"><h2 class="display-4">PUBG Mobile Global Championship</h2></a>
	            <p style="color:black">PUBG Mobile Global Championship atau PMGC merupakan creme de la creme-nya turnamen PUBG Mobile. Di sini, tim PUBG Mobile elit dari seluruh penjuru Dunia bertarung untuk mencari siapa yang terbaik di musim ini. Meskipun secara hadiah PMGC ini bukanlah turnamen dengan hadiah terbesar di PUBG Mobile namun secara prestige, turnamen inilah yang paling bergengsi di PUBG Mobile. Alasannya tentu saja karena peserta PMGC merupakan tim-tim terbaik dari setiap regional yang telah dibuktikan melalui gelar yang mereka dapatkan di musim tersebut.</p>
	          </div>
	        </div>
	      </div>
	    </div>
	  </section>
</main>	

	 <!-- FOOTER
    ================================================== -->	
		<section class="section footer-bottom">	
			<div class="container">
				<div class="sixteen columns">
					<p>© ALL RIGHTS RESERVED. MADE BY WIBU PROJECT</p>
				</div>	
			</div>
		</section>
 	

	<div class="scroll-to-top">&#xf106;</div>
	</div>
	
	
		
	<!-- JAVASCRIPT
    ================================================== -->
<script type="text/javascript" src="../js/jquery-2.1.1.js"></script>
<script type="text/javascript" src="../js/modernizr.custom.js"></script> 
<script type="text/javascript" src="../js/jquery.mobile.custom.min.js"></script>
<script type="text/javascript" src="../js/retina-1.1.0.min.js"></script>		
<script type="text/javascript" src="../js/jquery.animsition.min.js"></script>
<script type="text/javascript">
(function($) { "use strict";
	$(document).ready(function() {
	  
	  $(".animsition").animsition({
	  
		inClass               :   'zoom-in-sm',
		outClass              :   'zoom-out-sm',
		inDuration            :    1500,
		outDuration           :    800,
		linkElement           :   '.animsition-link', 
		loading               :    true,
		loadingParentElement  :   'body', 
		loadingClass          :   'animsition-loading',
		unSupportCss          : [ 'animation-duration',
								  '-webkit-animation-duration',
								  '-o-animation-duration'
								],
		overlay               :   false,
		overlayClass          :   'animsition-overlay-slide',
		overlayParentElement  :   'body'
	  });
	});  
})(jQuery);
</script>
<script type="text/javascript" src="../js/jquery.easing.js"></script>	
<script type="text/javascript" src="../js/jquery.hidescroll.min.js"></script>	
<script type="text/javascript">
	$('.header-top').hidescroll();
</script>
<script type="text/javascript" src="../js/smoothScroll.js"></script>
<script type="text/javascript" src="../js/jquery.parallax-1.1.3.js"></script>
<script type="text/javascript" src="../js/imagesloaded.pkgd.min.js"></script> 
<script type="text/javascript" src="../js/masonry.js"></script> 
<script type="text/javascript" src="../js/isotope.js"></script> 
<script type="text/javascript" src="../js/jquery.counterup.min.js"></script>
<script type="text/javascript" src="../js/waypoints.min.js"></script>
<script type="text/javascript" src="../js/scrollReveal.js"></script>
<script type="text/javascript">
(function($) { "use strict";
      window.scrollReveal = new scrollReveal();
})(jQuery);
</script>
<script type="text/javascript" src="../js/owl.carousel.min.js"></script>
<script type="text/javascript"> 
(function($) { "use strict";          
			jQuery(document).ready(function() {
				var offset = 450;
				var duration = 500;
				jQuery(window).scroll(function() {
					if (jQuery(this).scrollTop() > offset) {
						jQuery('.scroll-to-top').fadeIn(duration);
					} else {
						jQuery('.scroll-to-top').fadeOut(duration);
					}
				});
				
				jQuery('.scroll-to-top').click(function(event) {
					event.preventDefault();
					jQuery('html, body').animate({scrollTop: 0}, duration);
					return false;
				})
			});
})(jQuery);
</script>
<script type="text/javascript" src="../js/jquery.fitvids.js"></script>
<script type="text/javascript" src="../js/styleswitcher.js"></script>
<script type="text/javascript" src="../js/custom-ajax-home.js"></script>  
<script>
  function berhasilBerhasilBerhasilHore() {
    alert("Berhasil Membuat Akun!");
  }
</script>	  
<!-- End Document
================================================== -->
</body>
</html>